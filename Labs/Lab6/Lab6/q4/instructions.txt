<p>You may assume that the files <b>Data.h</b> and <b>Sortable.h</b> exist and that the <tt>Data</tt> class has been modified to work with <tt>Sortable</tt> objects.</p>

<p>Now create a class called <tt>Circle</tt> which inherits from <tt>Sortable</tt>. A <tt>Circle</tt> object will only have a radius. <tt>Circle</tt> objects are compared according to radius. <tt>Circle</tt> objects should be printed one per line as follows, Circle with radius: <i>x</i>, where <i>x</i> is the radius of the circle.</p>

<p>You can test your code with the file <a href='public/sortingCircles.cpp' target='_blank' class='exercise_title'><b>sortingCircles.cpp</b></a> as well as the <b>Sortable.h</b> file and the <b>Data.h</b> file that you submitted for the last question.</p>