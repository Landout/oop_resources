1.
Study the file refs.cpp. It makes use of a function named triple. Provide this function in a file named refs.h.

Sample out:
15

2.
Download the file catsDogs.zip. It contains the files catsDogs.cpp, Cat.h, Dog.h and Animal.h. The program does not compile in its current form. A piece of code is missing from Animal.h. Add that code and upload the new Animal.h.

Sample out:
Meow, meow!
Woof, woof!

3.
Download the file objects.cpp. It instantiates some Objects and prints out the value of the count variable, which simply keeps track of how many instances of Object have been created.

Your task is to implement the Object class. It only needs to have a static member named count, which should be incremented each time a new instance of the class is created. You should also provide the appropriate constructors. Do not worry about destructors for this exercise. Save your class in a file named Object.h

Sample out:
3

4.
In the previous question, where we were counting the objects, there were 3 instances. One of them was created because we passed the object into the function f by value. Modify the function so that this does not happen. Your program should report that there are 2 objects, not 3. Upload your modified version of objects.cpp

Sample out:
2

5.
You are required to complete the following tasks:

    Create a AppWindow class that will contain as member a rectangle class AppRect defining its position and size. AppWindow will have the following two constructors:

    /*! Constructor receives the window area size. */
    AppWindow ( int x, int y, int w, int h );
    AppWindow ( const AppRect& r );
    Write a method to retrieve the rectangle:

    const AppRect& AppWindow::rect();
    Write now the resize() virtual method as described below. Every time this method is called your implementation is supposed to change the coordinates of the internal rectangle of the window.

    /*! Method resize will be called by the system everytime the window is resized */
    virtual void AppWindow::resize ( int w, int h );
    Write now two classes deriving AppWindow: CircleWin, and RectWin. These classes will have correct constructors as needed and will override the resize method such that:

    - CircleWin will get the minimum dimension (among w and h) and will print it like this: "radius: <min>", where <min> is the minimum dimension;
    - RectWin will print the area like this: "area: <area>".

Upload your three classes as a single header file named Header.h. Your code will be tested for correctness using the file virtualMethods.cpp. 

sample out:
radius: 2
area: 8
area: 6
radius: 1

Mini-project.
You are now going to extend your previous freeglut application (of LA04) in the following way: instead of managing a simple list of Rects, you will be adding to the Rects more functionality making them behave like generic interactive graphical objects. You will be managing a list of graphical rects "GrRect", where each GrRect will draw itself, handle its own events, etc. For that you will add to GrRect the same virtual methods mentioned in the previous exercise for the class AppWindow.

Note: we will always be using the freeglut support code to build applications consisting of a single window. Inside your single-window application you will be managing several GrRects that will behave similarly to "windows", but everything will be always inside your single-window executable.

The functionality you will need to implement can be summarized as follows: a) As before, you will maintain a list of GrRect objects. You may use your Stash class, or the std::vector<> class. Each GrRect will implement its virtual methods according to the target functionality described below. Each GrRect will have its own rectangular coordinates and color. When your program starts, it will already display several GrRects randomly placed inside your application. Each GrRect will appear as a rectangular object in the screen.

b) As before, the order of your GrRect objects will determine which objects are on top of each other. When you draw the GrRects in order, you will call the virtual method draw() of each GrRect in order, so that the first ones will appear before (or below) the last ones.

c) Each time a mouse click is detected, you will check what is the GrRect that contains the mouse click: (i) nothing happens if no GrRect contains the mouse click; (ii) if the GrRect is not a top object, the object will be moved to become the top-most object, call focus(true) for it, and call focus(false) for the previous top GrRect; (iii) if the GrRect is already the top one, check if the mouse click is close to the top margin, and if it is, assume that the intent is to move the GrRect following the mouse, if it is not, send the event to the top GrRect by calling its method handle(), and let the top GrRect process its event.

d) Every time a GrRect receives an event, store in a vector the coordinates of the mouse event in local coordinates in respect to the top-left corner of the window. This means that your GrRect will store a vector of Vec classes (from LA04). Feel free to rename the Vec class with the naming convention that you prefer.

e) Whenever GlutWindow::draw() is called, you are going to call the draw() method of each GrRect in order, and each GrRect will draw the points stored inside its vector of points. Recall that the points are stored in local coordinates to each GrRect, so that when you draw the points, you should add their coordinates to the current top-left point of GrRect. In this way, when you drag the GrRect around, its points should move together with the GrRect. The points represent the content that the GrRect is managing. To obtain good results when many GrRects are overlapped, you should first paint GrRect's background with a rectangle, and then draw the points on top of the rectangle.

At this point you will be able to manipulate several graphical rectangle, add points to any of them, move them around, etc. The resize() method will be added later, it will allow you to change the size of each GrRect, and it will be implemented in the next lab assignment. 